import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App.vue'
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import VueFire from 'vuefire'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import { routes } from "./routes";

Vue.use(BootstrapVue)
Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(VueFire);

// Connection to firebase
Vue.http.options.root = 'https://vue-account-manager.firebaseio.com/';
Vue.http.interceptors.push((request, next) => {
  console.log(request);
  if(request.method == 'POST'){
    request.method = 'PUT';
  }

  next(response => {
    response.json = () => { return { messages: response.body} }
  });
});

// Router
const router = new VueRouter({
  mode: 'history',
  routes
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
