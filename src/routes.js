import Home from './components/Home.vue';
import Users from './components/users/Users.vue';
import Accounts from './components/accounts/Accounts.vue';
import Products from './components/products/Products.vue';
import EditProduct from './components/products/EditProduct.vue'
import EditUser from './components/users/EditUser.vue'
import EditAccount from './components/accounts/EditAccount.vue'

export const routes = [
  { path: '/', component: Home },
  { path: '/users', component: Users },
  { path: '/accounts', component: Accounts },
  { path: '/products', component: Products },
  {
    name: 'EditProduct',
    path: '/products/edit/:id',
    component: EditProduct
  },
  {
    name: 'EditUser',
    path: '/users/edit/:id',
    component: EditUser
  },
  {
    name: 'EditAccount',
    path: '/accounts/edit/:id',
    component: EditAccount
  },
]