import Firebase from 'firebase'

let config = {
  apiKey: 'AIzaSyA30UrNogNGoh5fBTZNbdHCDqtt0RXp2HE',
  authDomain: 'vue-account-manager.firebaseapp.com',
  databaseURL: 'https://vue-account-manager.firebaseio.com',
  projectId: 'vue-account-manager',
  storageBucket: 'vue-account-manager.appspot.com',
  messagingSenderId: '897874653292'
}

let app = Firebase.initializeApp(config)
export const db = app.database()
